<?php

namespace App\Http\Controllers;

use App\Http\Resources\SaleResource;
use App\Models\Sale;
use App\Models\User;
use App\Services\SaleService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SaleController extends Controller
{
    private SaleService $service;

    public function __construct()
    {
        $this->service = new SaleService;
    }

    public function getSale(Request $request)
    {
        return response()->json(new SaleResource($this->service->getSale()));
    }

    public function checkSale(Request $request)
    {
        $validated = $request->validate([
            'code' => 'nullable',
        ]);
        return response()->json($this->service->checkSale($validated['code']));
    }
}
