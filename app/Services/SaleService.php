<?php

namespace App\Services;

use App\Http\Resources\SaleResource;
use App\Models\Sale;
use Illuminate\Support\Str;

class SaleService
{
    public function getSale(): SaleResource
    {
        $user = auth()->user();

        if (!$user->sale_id || $user->sale->created_at->diffInHours(now()) > 0) {
            $sale = $this->createSale($user);
            return new SaleResource($sale);
        } else {
            return new SaleResource($user->sale);
        }
    }

    public function checkSale($code): array
    {
        $user = auth()->user();
        $sale = Sale::where('code', $code)->first();
        if (!$sale || $user->sale->id != $sale->id || $sale->created_at->diffInHours(now()) > 3) {
            return ['text' => 'Скидка недоступна'];
        } else {
            return ['text' => "Скидка для данного кода: $sale->percent%"];
        }
    }

    private function createSale($user)
    {
        $sale = Sale::create([
            'percent' => rand(1, 50),
        ]);
        $sale->update([
            'code' => Str::random(3) . $sale->id . Str::random(3)
        ]);
        $user->sale_id = $sale->id;
        $user->save();
        return $sale;
    }
}
