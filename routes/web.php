<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\SaleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->middleware('auth');
Route::group(['prefix' => 'sale'], function () {
    Route::post('get', [SaleController::class, 'getSale']);
    Route::post('check', [SaleController::class, 'checkSale'])->name('check-sale');
});
Auth::routes();
