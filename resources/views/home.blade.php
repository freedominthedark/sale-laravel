@extends('layouts.app')

@section('content')
    <div class="get-wrapper">
        <div>
            <button class="sale__btn js-get-sale" type="button">
                Получить скидку
            </button>
        </div>

    </div>
    <div class="check-wrapper">
            <div class="check-title">Проверка кода</div>
            <label for="code" class="code">Введите ваш код
                <input type="text" id="code" placeholder="Код">
            </label>
            <button class="check__btn" type="button" disabled>Проверить</button>
            <div class="response-text"></div>
    </div>
@endsection
