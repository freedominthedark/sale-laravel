
window.addEventListener("DOMContentLoaded", (event) => {
    const saleBtn = document.querySelector('.sale__btn');
    const getWrapper = document.querySelector('.get-wrapper')
    saleBtn.addEventListener('click', () => {
        axios.post('/sale/get', {})
            .then((response) => {
                getWrapper.innerHTML = '';
                getWrapper.innerHTML =
                    `<div>
                        <div>Ваша скидка: ${response.data.percent}%</div>
                        <div>Код скидки: ${response.data.code}</div>
                    </div>`
            })
    })

    const checkBtn = document.querySelector('.check__btn');
    const inputCode = document.querySelector('#code')
    inputCode.addEventListener('input', () => {
        if (inputCode.value !== '') {
            checkBtn.removeAttribute('disabled')
        }else {
            checkBtn.setAttribute('disabled', 'true')
        }
    })
    checkBtn.addEventListener('click', () => {
        axios.post('/sale/check', {
            code: inputCode.value
        })
            .then((response) => {
               document.querySelector('.response-text').textContent = response.data.text
            })
    })
});
